package com.example.demo.repository;

import com.example.demo.model.User;

import java.util.HashMap;
import java.util.Map;

public class UserRepository {

    private final Map<Integer, User> usersDatabase;

    public UserRepository() {
        usersDatabase = new HashMap<>();

        usersDatabase.put(1, new User("cracker", "cracker1234", true, 0));
        usersDatabase.put(2, new User("marry", "marietta!#09", true, 0));
        usersDatabase.put(3, new User("silver", "$silver$", true, 0));
    }

    public boolean checkLogin(final String login, final String password) {

        return usersDatabase.entrySet().stream().anyMatch( row -> {
            //login found
            if(row.getValue().getLogin().equals(login)){
                //isActive equals true means allowed to log in
                if(row.getValue().isActive()){
                    //right password
                    if (row.getValue().getPassword().equals(password)){
                        row.getValue().setIncorrectLoginCounter(0);
                        return true;
                    //wrong password
                    } else {
                        if(row.getValue().getIncorrectLoginCounter()<2){
                            row.getValue().setIncorrectLoginCounter(row.getValue().getIncorrectLoginCounter()+1);
                        } else {
                            row.getValue().setActive(false);
                        }
                        return false;
                    }
                //isActive equals false means can not log in, throws exception
                } else {
                    throw new IllegalStateException("Password was already entered wrong 3 times!");
                }
            }
            //login not found
            return false;
        });

    }
}
